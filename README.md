# Today's Mood

## Project setup
Using Docker is the recommended way to run this project.

### Using Docker
#### Build project
Run only once to build images of frontend and backend and run all Docker
containers.
```
sudo docker-compose up --build
```

#### Running the project
Run all Docker containers
```
sudo docker-compose up
```

Shutdown all Docker containers 
```
sudo docker-compose down
```

#### Shutdown all containers and volumes 
This is useful if there are some bugs on database, ruby gems and/or
node\_modules.
```
sudo docker-compose down -v
```

### Without Docker
You don't need Docker to run this project. But you will need PostgreSQL,
Node.js and Ruby on your system. These are the versions tested with Today's
Mood.

- PostgreSQL 10.7
- Node.js 10.15.3
- Ruby 2.5.1
- Rails 5.2.3
- Bundler 2.0.1

#### Backend
On PostgreSQL, you should have a database named `todaysmood`. Your
current user should be able to read and write this database. You can
change this settings in `config/database.yml`.

Now that PostgreSQL is setup and running, we can start the rails server.

On the first time, we need to install dependencies:
```
cd backend
bundler install
bundler exec rails db:migrate
```

Then, we can run the following command to start the server:
```
bundler exec rails server
```

#### Frontend
Before running the project, we need to install dependencies:
```
cd frontend
npm install
```

Then, we can run the following command to start the server:
```
npm run serve
```

Now you should be able to access the app in
[http://localhost:8080/](http://localhost:8080/)

## Running unit tests

To run tests, use the following command:
```
cd backend
rspec
```
