import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './assets/styles/tailwind.css'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faTasks,
  faUser,
  faBars,
  faPlusCircle,
  faAngleRight,
  faAngleLeft,
  faSearch,
  faFilter,
  faInfo,
  faInfoCircle,
  faPencilAlt,
  faTimes,
  faTimesCircle,
  faUndoAlt
} from '@fortawesome/free-solid-svg-icons'
import { faCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Vuelidate from 'vuelidate'
import VueSwal from 'vue-swal'

library.add(
  faTasks,
  faUser,
  faBars,
  faPlusCircle,
  faAngleRight,
  faAngleLeft,
  faSearch,
  faFilter,
  faInfo,
  faInfoCircle,
  faPencilAlt,
  faTimes,
  faCheckCircle,
  faCircle,
  faTimesCircle,
  faUndoAlt
)

Vue.use(Datetime)
Vue.use(Vuelidate)
Vue.use(VueSwal)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
