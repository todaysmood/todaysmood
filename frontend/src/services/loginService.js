import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API + '/api/v1',
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  login(user) {
    return apiClient.post('/auth/login', user)
  },
  postUser(user) {
    return apiClient.post('/auth/signup', user)
  }
}
