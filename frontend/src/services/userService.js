import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API + '/api/v1',
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getLists() {
    return apiClient.get(`users/${localStorage.getItem('user_id')}/lists`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
  },
  mutateList(list_id, data) {
    return apiClient.put(
      `users/${localStorage.getItem('user_id')}/lists/${list_id}`,
      data,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
  },
  createList(data) {
    return apiClient.post(
      `users/${localStorage.getItem('user_id')}/lists`,
      data,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
  },
  deleteList(list_id) {
    return apiClient.delete(
      `users/${localStorage.getItem('user_id')}/lists/${list_id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
  },
  mutateTask(task_id, data) {
    return apiClient.put(`tasks/${task_id}`, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
  },
  createTask(list_id, data) {
    data['list_id'] = list_id
    return apiClient.post('tasks', data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
  },
  forgotPassword(email) {
    return apiClient.post('forgot', { email })
  },
  resetPassword(newPass, token) {
    return apiClient.post('reset', {
      password: newPass,
      token: token
    })
  },
  validateToken(token) {
    return apiClient.post('reset/token', { token })
  }
}
