import Vue from 'vue'
import Vuex from 'vuex'
import loginService from '@/services/loginService.js'
import userService from '@/services/userService.js'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      name: '',
      email: ''
    },
    isLogged: localStorage.getItem('token') ? true : false,
    list: {
      listName: '',
      showTasks: false,
      listData: ''
    },
    task: {},
    showMenu: false,
    completedTasks: []
  },
  mutations: {
    ADD_USER(state, user) {
      state.user.name = user.name
      state.user.email = user.email
    },
    LOGIN_USER(state) {
      state.isLogged = true
    },
    LOGOUT_USER(state) {
      state.isLogged = false
      state.showMenu = false
    },
    TOGGLE_TASKS(state, listInfo) {
      if (state.list.showTasks) router.go(-1)

      state.list.showTasks = !state.list.showTasks
      state.list.listName = state.list.showTasks ? listInfo.name : ''
      state.list.listData = state.list.showTasks ? listInfo.listData : ''
    },
    TOGGLE_MENU(state) {
      state.showMenu = !state.showMenu
    },
    MUTATE_LIST(state, listData) {
      state.list.listData = listData
    },
    MUTATE_TASK(state, taskData) {
      state.task = taskData
    },
    ADD_COMPLETED_TASKS(state, completedTasks) {
      state.completedTasks = completedTasks
    },
    REMOVE_COMPLETED_TASK(state, taskId) {
      state.completedTasks = state.completedTasks.filter(task => {
        return task.id !== taskId
      })
    },
    FILTER_TASKS(state) {
      state.list.listData.tasks.sort((ta, tb) => {
        return tb.priority - ta.priority
      })
    }
  },
  actions: {
    createUser({ commit }, user) {
      return loginService.postUser(user).then(() => {
        commit('ADD_USER', { name: user.name, email: user.email })
      })
    },
    async userLogin({ commit }) {
      commit('LOGIN_USER')
    },
    userLogout({ commit }) {
      localStorage.removeItem('token')
      localStorage.removeItem('user_id')

      commit('LOGOUT_USER')
    },
    toggleTasks({ commit }, listInfo) {
      commit('TOGGLE_TASKS', listInfo)
    },
    toggleMenu({ commit }) {
      commit('TOGGLE_MENU')
    },
    mutateList({ commit }, listData) {
      commit('MUTATE_LIST', listData)
    },
    mutateTask({ commit }, taskData) {
      commit('MUTATE_TASK', taskData)
    },
    addCompletedTasks({ commit }, completedTasks) {
      commit('ADD_COMPLETED_TASKS', completedTasks)
    },
    async removeCompletedTask({ commit }, taskId) {
      const res = await userService.mutateTask(taskId, { completed: false })

      if (res.status === 200) commit('REMOVE_COMPLETED_TASK', taskId)
      else {
        this.$swal({
          title: 'Something bad happened',
          icon: 'error'
        })
      }
    },
    filterTasks({ commit }) {
      commit('FILTER_TASKS')
    }
  }
})
