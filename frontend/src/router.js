import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import SignUp from './views/SignUp.vue'
import Login from './views/Login.vue'
import PasswordChange from './views/PasswordChange.vue'
import PasswordReset from './views/PasswordReset.vue'
import Terms from './views/Terms.vue'
import NotFound from './views/NotFound.vue'
import UserLists from './views/UserLists.vue'
import UserArchive from './views/UserArchive.vue'
import UserTasks from './views/UserTasks.vue'
import ListEdit from './views/ListEdit.vue'
import TaskEdit from './views/TaskEdit.vue'
import userService from '@/services/userService.js'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/password',
      redirect: '/login'
    },
    {
      path: '/reset/:id',
      name: 'passwordChange',
      component: PasswordChange,
      beforeEnter: (to, from, next) => {
        userService
          .validateToken(to.params.id)
          .then(resp => {
            if (to.params.id !== null && resp.status === 200) next()
            else next('/404')
          })
          .catch(() => {
            next('/404')
          })
      }
    },
    {
      path: '/password/reset',
      name: 'passwordReset',
      component: PasswordReset
    },
    {
      path: '/terms',
      name: 'terms',
      component: Terms
    },
    {
      path: '/user',
      name: 'user',
      component: UserLists,
      meta: {
        tokenRequired: true
      }
    },
    {
      path: '/user/archive',
      name: 'archive',
      component: UserArchive,
      beforeEnter: (to, from, next) => {
        if (to.path === '/user/archive' && from.path === '/user') next()
        else next('/user')
      }
    },
    {
      path: '/user/tasks',
      name: 'tasks',
      component: UserTasks,
      beforeEnter: (to, from, next) => {
        if (
          to.path === '/user/tasks' &&
          (from.path === '/user' || from.path === '/user/tasks/edit')
        )
          next()
        else next('/user')
      }
    },
    {
      path: '/user/edit',
      name: 'list-edit',
      component: ListEdit,
      beforeEnter: (to, from, next) => {
        if (to.path === '/user/edit' && from.path === '/user') next()
        else next('/user')
      }
    },
    {
      path: '/user/tasks/edit',
      name: 'tasks-edit',
      component: TaskEdit,
      beforeEnter: (to, from, next) => {
        if (to.path === '/user/tasks/edit' && from.path === '/user/tasks')
          next()
        else next('/user')
      }
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
      props: true
    },
    {
      path: '*',
      redirect: { name: '404', params: { resource: 'page' } }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.tokenRequired) {
    const token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})

export default router
