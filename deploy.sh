cd /opt/todaysmood/
git pull origin prod
cd frontend

echo "Building frontend"
npx vue-cli-service build

echo "Copying static files to be served"
cp -r /opt/todaysmood/frontend/dist /opt/todaysmood/live

echo "Restarting backend"
sudo systemctl restart puma.service
