\documentclass[12pt]{article}

\usepackage[a4paper, total={6.5in, 9.5in}]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[portuguese]{babel}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{parskip}
\usepackage{indentfirst}

\setlength{\parindent}{18pt}
%\usepackage{showframe}% just to show page geometry, not needed
\graphicspath{{./src/plots/}}

\begin{document}

% ========== Edit your name here
\author{
    Rodrigo Orem\\
    \texttt{8921590}
    \and
    Thiago Teixeira\\
    \texttt{10736987}
    \and
    Washington Luiz\\
    \texttt{10737157}
    \and
    Wander da Souza\\
    \texttt{10737011}
    \and
    Ygor Tavela\\
    \texttt{10687642}
}

\title{
Relatório da Entrega Final do Projeto
\center{\Large{Today's Mood - TecProg 2}}
}
\maketitle

Link do app: \url{https://todaysmood.site}

Repositório do projeto: \url{https://gitlab.com/todaysmood/todaysmood/}

Nosso aplicativo é um gerenciador de tarefas inteligente. Na
proposta inicial, desejávamos ter um algoritmo que sugerisse tarefas
para o usuário baseado em: prioridade, tempo necessário para
realizar a tarefa e tempo não alocado que a pessoa tem.

A funcionalidade básica, de agenda, foi plenamente implementada.  O
algoritmo de sugestão não foi implementado, porque as funcionalidades
básicas já deram trabalho suficiente para nos deixar bastante ocupados.
No entanto, a interface e a modelagem de dados foi desenhada pensando no
projeto completo. Dessa forma, acreditamos ser completamente possível
continuar o trabalho a partir daqui, pois ele está estruturalmente
pronto para o que planejamos.

\section{Frontend} \label{frontend}

Optamos por usar Vue.js no frontend. Conforme explicado no segundo
relatório, queríamos que a interface do nosso projeto fosse reativa e
que ele pudesse navegar nas tarefas sem ter que recarregar a página.

Além disso, frameworks de frontend como o Vue.js permitem modularização
fácil do código, e graças a transpiladores e minificadores executados
pelo Webpack, conseguimos entregar um código otimizado usando
funcionalidades novas do JavaScript. É possível, inclusive, usar
funcionalidades novas ainda sem suporte dos navegadores atuais, graças a
\textit{polyfills} inseridos automaticamente pelo Webpack.

Utilizamos também o Vue Router e o Vuex, para reconhecer rotas de URL e
para gerenciar o estado da aplicação, respectivamente. Uma grande
vantagem de desenvolver com o Vue é o controle de estado: antes desses
frameworks, alterávamos o DOM (Document Object Model) manualmente quando
os dados mudavam. Isso dava um trabalho dobrado: tínhamos que alterar as
variáveis internas e garantir que a interface estava consistente com
elas.

Mas com o Vue e com o Vuex, nós apenas descrevemos as mudanças de estado
e o framework se encarrega de garantir que a interface representa aquele
estado, fazendo a menor quantidade de alteração possível no DOM. Para
isso é necessário também escrever os templates de maneira declarativa.

Trabalhamos também na implementação de notificações via WebSocket. Elas
seriam usadas para avisar o usuário quando uma tarefa chegou no prazo de
entrega, ou quando o usuário pediu para ser lembrado dela. No backend, a
implementação disso ficou quase pronta. No entanto, essa funcionalidade
não foi entregue devido à restrições de tempo e dificuldade de fazer o
deploy, que será comentado em mais detalhes na seção Deployment.

No design, demos prioridade ao tamanho para celulares. Em desktops,
recomendamos ativar o Modo Responsivo do navegador para visualizar o
layout da maneira como ele foi planejado. Seguem algumas capturas de
tela da aplicação:

\begin{figure}[ht!]
    \centering
    \subfloat[Login]
    {{\includegraphics[width=7cm]{images/login.png}}}%
    \qquad
    \subfloat[Listas de tarefas]
    {{\includegraphics[width=7cm]{images/lists.png}}}%
    \caption{Capturas de tela do Today's Mood}%
    \label{fig:example}%
\end{figure}

\begin{figure}[ht]
    \centering
    \subfloat[Lista de Tarefas -- Inbox]
    {{\includegraphics[width=7cm]{images/tasks.png}}}%
    \qquad
    \subfloat[Criar nova tarefa]
    {{\includegraphics[width=7cm]{images/new-task.png}}}%
    \caption{Capturas de tela do Today's Mood}%
    \label{fig:examples}%
\end{figure}

Em geral, ficamos muito satisfeitos com o desenvolvimento usando essas
tecnologias.

\section{Backend}

Como nós criamos o frontend usando o \textit{tooling} padrão da
indústria (\texttt{vue-cli}), tivemos problemas com a integração com o
Rails. É possível usar Vue.js com Rails facilmente, basta incluir um
script no HTML, porém o \textit{tooling} usado requer uma estrutura de
pastas diferente da usada pelo Rails. Há \textit{gems} que se propõem a
resolver esse problema, mas nós achamos que seria mais simples deixar o
frontend completamente separado do backend.

Uma vantagem dessa abordagem é que tal desacoplamento nos permite manter
mais de um frontend, por exemplo: um para web, outro para um desktop,
outro como chatbot do Telegram. O backend é, portanto, apenas uma API.
Frontend e backend compartilham o mesmo repositório, mas ficam em pastas
pastas diferentes.

Criar um projeto como API no Rails tem algumas consequências, por
exemplo há menos middlewares do que o padrão: não há middleware de
gerenciamento de cookies, além disso não há \textit{views},
\textit{helpers} e \textit{assets} quando criamos novos
\textit{resources}.

A autenticação é feita usando JSON Web Token (JWT). Como o frontend e o
backend ficam em domínios separados (afinal são projetos separados), se
a API enviar um cookie de autenticação para \url{api.todaysmood.site}, o
frontend em \url{todaysmood.site} será incapaz de ler esse cookie, por
estar em um domínio diferente. Com o JWT, nós salvamos o token de
autenticação com \texttt{localStorage}, assim conseguimos verificar se o
usuário é válido e está logado.

Além disso, há confirmação de cadastro por email. Enquanto o email não
for confirmado, não será possível logar. Essa funcionalidade causou
certo problema na segunda entrega, pois se o envio falhar o
funcionamento da aplicação é comprometida, já que não será possível
logar. Mas esses problemas foram plenamente resolvidos. Para que
o envio de emails funcione, é necessário configurar uma conta de email
através das variáveis de ambiente \texttt{GMAIL\_EMAIL} e
\texttt{GMAIL\_PASSWORD}. É possível, no ambiente de desenvolvimento,
completar a confirmação do cadastro sem usar emails: basta olhar no
console do Rails após enviar o formulário de cadastro. Lá, o corpo da
mensagem de email deve ser exibido, e será possível clicar no link
usando a saída desse console.

\section{Testes automatizados}

Utilizamos o RSpec para os unit tests. Os testes ficam na pasta
\texttt{backend/spec}. Para rodar os testes, basta executar o comando
\texttt{rspec} no diretório \texttt{backend}. A figura \ref{fig:rspec}
mostra a saída de um desses testes.

\begin{figure}[ht!]
    \fbox{\includegraphics[width=0.95\textwidth]
        {images/rspec.png}
    }
    \caption{\label{fig:rspec} Execução dos testes de \textit{List} com
    RSpec}
\end{figure}

No nosso projeto, implementamos integração contínua (CI). Isso significa
que os testes rodam automaticamente no GitLab quando fazemos qualquer
commit na branch master. É possível acompanhar o resultado dos testes
apenas olhando na página do repositório em
\url{https://gitlab.com/todaysmood/todaysmood}. Isso é bastante útil
para verificar se novos commits introduzem bugs de regressão, além de
ser útil para verificar o progresso da implementação de determinada
funcionalidade no Test Driven Development (TDD) -- embora não tenhamos
utilizado essa metologia no projeto.

\section{Deployment}

Utilizamos um Virtual Private Server (VPS) na
\href{https://www.digitalocean.com}{Digital Ocean} para fazer o
deployment. Obtivemos 50 dólares em crédito através do GitHub Student
Pack, e pegamos a VPS mais humilde, com 1GB de RAM e 25 GB de disco SSD
por \$5/mês (para comparação, a versão gratuita do Heroku oferece 512MB
de RAM). Com os créditos obtidos, será possível rodar continuamente o
projeto por 10 meses sem pagar nada.

Além de integração contínua (CI), nosso projeto possui desenvolvimento
contínuo (CD) utilizando o GitLab CI/CD. Isso significa que o deploy é
feito automaticamente. Adicionalmente à \texttt{master}, temos a branch
\texttt{prod}, cujos commits são enviados para o servidor em produção. A
pipeline da branch \texttt{prod} roda os testes de unidade primeiro, e
caso eles passem, roda o deploy, que instrui o servidor a obter a última
versão do Today's Mood via Git, faz o build otimizado do frontend e
reiniciando o servidor do backend.

\begin{figure}[ht!]
    \fbox{\includegraphics[width=0.95\textwidth]
        {images/pipeline.png}
    }
    \caption{\label{fig:pipeline} Pipeline CI/CD no GitLab}
\end{figure}

Uma das orientações da disciplina era usar o Heroku para o deploy, mas
ele não foi capaz de suprir nossas necessidades. Uma das nossas
funcionalidades era o envio de notificações associadas a tarefas,
conforme descrito na seção \ref{frontend}. Para isso, utilizamos o
framework \textit{Active Job} do Rails, que permite agendar a execução
de códigos. O problema é que isso requer um processo que fique
executando o tempo todo no servidor. O plano gratuito do Heroku não
permite que um \textit{Dyno} execute continuamente o tempo todo, pois
há um limite de horas mensais. Além disso, os \textit{web dynos} entram
no modo sleep se não receberem tráfego durante um período de 30 minutos.

Devido a esses problemas que encontramos ao fazer o deploy com o Heroku,
que também atrasaram o desenvolvimento da funcionalidade de
notificações, decidimos procurar alternativas. A VPS nos ofereceu mais
flexibilidade: embora tenhamos escolhido a DigitalOcean, a configuração
para rodar nosso aplicativo funciona em qualquer provedor que ofereça
Linux. Com o Heroku, é possível fazer configurações mais complexas
através dos buildpacks, mas essas configurações são muito específicas e
não funcionariam em outros provedores PaaS.


\section{Divisão do trabalho}
Em relação à participação de cada membro na última fase:

\begin{itemize}

    \item Rodrigo implementou CI/CD no projeto, fez adaptações para o
        deployment e configurou o servidor do app. Além disso, escreveu
        boa parte deste relatório.

    \item Thiago fez o design da landing page, desenhou o logo do
        projeto, e contribuiu na realização do relatório.

    \item Wander fez a documentação das rotas e desenvolvimento da
        bateria de testes nos modelos.

    \item Washington realizou pequenas refatorações de código no
        backend, implementação da recuperação de senha, com uso de token
        e email de verificação, com redirecionamento para a tela de
        recadastro de senha no frontend.

    \item Ygor trabalhou no desenvolvimento do frontend em geral,
        partindo desde a construção das telas com HTML/CSS/JS, à
        comunicação com o backend para o funcionamento pleno do app.

\end{itemize}


\end{document}
