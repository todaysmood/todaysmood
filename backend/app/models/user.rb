class User < ApplicationRecord
    before_create :confirmation_token
    has_many :team_users, dependent: :destroy
    has_many :teams, through: :team_users
    has_many :tags, :as => :taggable, dependent: :destroy
    has_many :lists, :as => :listable, dependent: :destroy
    has_one_attached :avatar 
    has_secure_password
    
    before_save :downcase_fields
    validates :name, presence: true, length: {minimum:1, maximum:64}
    validates :email, presence: true, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :password,
            length: { minimum: 1 },
            if: -> { new_record? || !password.nil? }

                
    def email_activate
        self.email_confirmed = true
        self.confirm_token = nil
        save!(:validate => false)
    end

    def generate_password_token!
        self.reset_password_token = generate_token
        self.reset_password_sent_at = Time.now.utc
        save!
    end
       
    def password_token_valid?
        (self.reset_password_sent_at + 4.hours) > Time.now.utc
    end
       
    def reset_password!(password)
        self.reset_password_token = nil
        self.password = password
        save!
    end

    private
    def downcase_fields
        self.name.downcase!
        self.email.downcase!
    end
   
    def confirmation_token
        if self.confirm_token.blank?
            self.confirm_token = SecureRandom.urlsafe_base64.to_s
        end
    end
    
    def generate_token
        SecureRandom.hex(10)
    end
 
                
end
