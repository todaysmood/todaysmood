class Reminder < ApplicationRecord
    belongs_to :task

    validates :title, presence: true, length: {minimum:1, maximum:64}
    validates :description, length: {minimum:0, maximum:128}
end
