class Attachment < ApplicationRecord
    belongs_to :task
    has_one_attached :attachment
    
    validates :title, presence: true, length: {minimum:0, maximum:64}
end
