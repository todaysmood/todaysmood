class Tag < ApplicationRecord
    belongs_to :taggable, :polymorphic => true
    has_many :tag_tasks
    has_many :tasks, through: :tag_tasks

    validates :title, presence: true, length: {minimum:0, maximum:64}
    validates :description, length: {minimum:0, maximum:128}
end
