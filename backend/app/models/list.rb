class List < ApplicationRecord
    belongs_to :listable, :polymorphic => true
    has_many :tasks, dependent: :destroy
    
    validates :title, presence: true, length: {minimum:1, maximum:16}
    validates :description, length: {minimum:0, maximum:128}
end