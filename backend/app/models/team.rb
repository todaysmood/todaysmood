class Team < ApplicationRecord
    has_many :team_users
    has_many :users, through: :team_users
    has_many :lists, :as => :listable, dependent: :destroy
    has_many :tags, :as => :taggable, dependent: :destroy

    validates :title, presence: true, length: {minimum:1, maximum:16}
    validates :description, length: {minimum:0, maximum: 128}

end