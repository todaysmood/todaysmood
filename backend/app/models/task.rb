class Task < ApplicationRecord
    has_many :tag_tasks, dependent: :destroy
    has_many :tags , through: :tag_tasks
    has_many :reminders, dependent: :destroy
    has_many :attachments, dependent: :destroy
    belongs_to :list
    # has_many :list_tasks
    # has_many :lists, through: :list_tasks

    validates :title, presence: true, length: {minimum:1, maximum:100}
    validates :description, length: {minimum:0, maximum:512}
    validates :priority, numericality: {only_integer: true}
    validates :completed, inclusion: {in: [true, false]}
    
    # validates :estimated_time
    
end
