class Api::V1::TeamSerializer < ActiveModel::Serializer
  attributes :id, :title
  has_many :lists
  has_many :users, through: :team_users
end
