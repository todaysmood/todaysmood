class Api::V1::UserSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :name, :email, :avatar_url
  has_many :lists
  has_many :teams
  has_many :tags

 
  def avatar_url
    if object.avatar.attached?
      variant = object.avatar.variant(resize: "100x100") 
      return rails_representation_url(variant, only_path: true) 
    end
  end
end
