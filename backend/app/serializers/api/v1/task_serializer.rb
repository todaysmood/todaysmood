class Api::V1::TaskSerializer < ActiveModel::Serializer
    attributes :id, :title, :description, :due, :estimated_time, :priority, :completed
    has_many :reminders
    has_many :attachments
    has_many :tags
  end
  