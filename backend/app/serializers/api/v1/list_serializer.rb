class Api::V1::ListSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
  has_many :tasks
end
