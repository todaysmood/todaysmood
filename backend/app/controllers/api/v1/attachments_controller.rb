class Api::V1::AttachmentsController < Api::V1::ApiController
    def index
        task = Task.find(params[:task_id])
        render json: task.attachments
    end
    def create
        task = Task.find(params[:task_id])
        attach = task.attachments.create(attachment_params)
        if attach.save
            render json: {message: url_for(attach.attachment), status: :created}, status: :created
        else
            render json: {status: :no_content}
        end
    end

    def destroy
        task = Task.find(params[:task_id])
        attach = task.attachments.find(params[:id])
        if attach.attachment.purge_later
            attach.destroy
            render json: {message: "Attachment has been purged", status: :ok}, status: :ok
        end
    end

    def show
        task = Task.find(params[:task_id])
        attach = task.attachments.find(params[:id])
        render json: {message: url_for(attach.attachment), status: :ok}, status: :ok if attach.attachment.attached?
    end

    private

    def attachment_params
        params.permit(:title, :task_id, :attachment)
    end

end
