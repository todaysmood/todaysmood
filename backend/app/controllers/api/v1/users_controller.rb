class Api::V1::UsersController < Api::V1::ApiController
    skip_before_action :authenticate_request, only: %i[login create confirm_email forgot reset change_password validate_token]

    resource_description do
        name "Users"
    end

    api :POST, '/v1/auth/login/', "Inicia uma sessão para o usuário"
    param :email, String, :desc => "Email do usuário" , :required => true
    param :password, String, :desc => "Senha do usuário" , :required => true
    def login
        authenticate params[:email], params[:password]
    end


    api :GET , '/v1/users/', "Lista todos os usuários"
    def index
        users = User.all
        render json: users, status: :success
    end


    api :POST, '/v1/auth/signup', "Cria um usuário"
    param :name, String, :desc => "Nome do usuário", :required => true
    param :email, String, :desc => "Email do usuário" , :required => true
    param :password, String, :desc => "Senha do usuário" , :required => true
    param :avatar, String, :desc => "Avatar do usuário" , :required => false
    def create
        user = User.create(user_params)
        # if !user.avatar.attached?
        #     user.avatar.attach(io: File.open('rails/app/assets/images/avatar.png'), filename: 'avatar.png', content_type: 'image/png')
        # end
        if user.save
            user.lists.create({title: 'inbox', description: 'Standard list'})
            Api::V1::UserMailer.with(user: user).welcome_email.deliver_later
            render json: {:data => user, status: :created}, status: :created
        else
            render json: {:data => user.errors, status: :not_acceptable}, status: :not_acceptable
        end
    end


    api :GET, '/v1/users/:id', "Lista um usuário determinado"
    param :id, :number, :desc => "id do usuário", :required => true
    def show
        user = User.find(params[:id])
        render json: user
    end

    def update
        user = User.find(params[:id])
        if user.update(user_params)
            render json:user
        end
    end

    def destroy
        user = User.find(params[:id])
        if user.destroy
            render json: user
        end
    end

    api :GET, '/v1/users/:id/teams', "Lista os times dos quais o usuário faz parte"
    param :id, :number, :desc => "id do usuário", :required => true
    def user_teams
        user = User.find(params[:user_id])
        render json: user.teams
    end


    api :GET, 'v1/users/:id/tasks_completed', "Lista as tarefas concluidas pelo usuário"
    param :id, :number, :desc => "id do usuário", :required => true
    def user_tasks_completed
        lists_ids= User.find(params[:user_id]).lists.ids
        tasks = Task.where({completed: true, list_id: [lists_ids]})
        render json: tasks
    end


    api :GET, 'v1/confirm_email/:token', "Verifica o email do usuário"
    param :token, :number, :desc => "Token do usário", :required => true
    def confirm_email
        user = User.find_by_confirm_token(params[:token])
        if !user.nil?
          user.email_activate
          render plain: "Thanks for registring!"
        else
          render plain: "Invalid email or password"
        end
    end

    def forgot
        if params[:email].blank?
            return render json: {error: 'email not present'}
        end

        user = User.find_by(email: params[:email])

        if user.present?
            user.generate_password_token!
            Api::V1::UserMailer.with(user: user).password_reset_email.deliver_now
            render json: {message: 'reset password requested' ,status: 'ok'}, status: :ok
        else
            render json: {error: 'Email not found'}, status: :not_found
        end
    end

    def reset
        
        if params[:token].blank?
            return render json: {error: 'Token not present', status: :not_found}, status: :not_found
        end

        token = params[:token].to_s
        
        user = User.find_by(reset_password_token: token)
        if user.present? && !params[:email].to_s == user.email.to_s
            render json: {message: 'Invalid email', status: :not_found}, status: :not_found
        elsif user.present? && user.password_token_valid?
            user.reset_password!(params[:password])
            render json: {message: 'Password has been changed', status: :ok}, status: :ok
        else
            render json: {message: 'Token has been expired. Send another request', status: :not_found}, status: :not_found
        end
    end

    def validate_token
        token = params[:token].to_s
        if token.present? && User.find_by(reset_password_token: token)
            render json: {message: 'Token is valid', status: :ok}, status: :ok
        else
            render json: {message: 'Token is blank or invalid', status: :not_found}, status: :not_found
        end
    end

    private

    def user_params
        return params.permit(:name, :email, :password, :avatar)
    end

    def authenticate(email, password)
        command = AuthenticateUser.call(email, password)
        user = User.find_by_email(email)
        if user.nil?
            render json: {error: "Email or password are incorrect", status: :not_found}, status: :not_found
        elsif !user.email_confirmed?
            render json: {error: "Email not confirmed", status: :unauthorized}, status: :unauthorized
        else command.success?
            render json: {
                access_token: command.result,
                user_id: user.id,
                status: :ok
            },
            status: :ok
        end
    end

end
