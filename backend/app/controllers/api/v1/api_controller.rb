class Api::V1::ApiController < ApplicationController
    skip_before_action :verify_authenticity_token

    def get_owner
        if params.has_key? :user_id
            owner = User.find(params[:user_id])
        elsif params.has_key? :team_id
            owner = Team.find(params[:team_id])
        end
    end
end