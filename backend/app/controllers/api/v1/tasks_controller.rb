class Api::V1::TasksController < Api::V1::ApiController

    resource_description do
        name "Tasks"
    end

    def index
        list = List.find(params[:list_id])
        render json: list.tasks
    end

    def create
        task = Task.create(task_params)
        if task.save
            render json: task
        else
            render plain: "NOT SAVED"
        end
    end

    def show
        task = Task.find(params[:id])
        render json: task
    end

    def destroy
        task = Task.find(params[:id])
        if task.destroy
            render json: task
        else
            render plain: "TASK NOT DESTROYED"
        end
    end

    def update
        task = Task.find(params[:id])
        task.update(task_params)
        if task.save
            render json: task
        end
    end

    api :DELETE , '/v1/remove_tag/:tag_id', "Remove a tag de uma determinada task"
    param :task_id, :number, :desc => "Id da tarefa", :required => true
    param :id, :number, :desc => "Id da tag", :required => true
    def remove_tag
        task = Task.find(params[:task_id])
        tag = task.tags.find(params[:id])
        task.tags.delete(tag) if tag
    end

    def add_tag
        task = Task.find(params[:task_id])
        tag = task.tags.create()
    end

    private
        def task_params
            params.delete :user_id if params.has_key? :user_id
            params.delete :team_id if params.has_key? :team_id
            params.permit(:title, :description, :due, :estimated_time, :priority, :completed, :list_id)
        end
end
