class Api::V1::TagsController < Api::V1::ApiController
    def index
        if params.has_key? :user_id or params.has_key? :team_id
            tags = get_owner.tags
        else
            tags = Tag.all
        end
        render json: tags
    end

    def create
        owner = get_owner
        tag = get_owner.tags.create(tags_params)
        if tag.save
            render json: tag
        else
            render plain: "NOT SAVED"
        end
    end

    def show
        user = get_owner
        render json: user.tags.find(params[:id])
    end

    def update
        tag = get_owner.tags.find(params[:id])
        tag.update(tags_params)
        if tag.save
            render json: tag
        end
    end

    def destroy
        tag = get_owner.tags.find(params[:id])
        if tag.destroy
            render json: tag
        end
    end

    private
        def tags_params
            params.permit(:title, :description)
        end
end
