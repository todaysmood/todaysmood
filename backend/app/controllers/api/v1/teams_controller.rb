class Api::V1::TeamsController < Api::V1::ApiController
    
    resource_description do 
        name "Teams"
    end

    def index
        teams =  Team.all
        render json: teams
    end

    def create
        team = Team.create(team_params)
        if team.save
            render json: team
        end
    end

    def show
        team = Team.find(params[:id])
        if team
            render json: team
        end
    end

    def update
        team = Team.find(params[:id])
        if team.update(team_params)
            render json: team
        end
    end

    def destroy
        team = Team.find(params[:id])
        if team.destroy
            render json: team
        end
    end

    api :POST, '/v1/add_user/:user_id', "Adiciona um usuário a um determinado time"
    param :user_id, :number, :desc => "Id do usuário" , :required => true
    param :team_id, :number, :desc => "Id do time" , :required => true
    def add_user
        user = User.find(params[:user_id])
        team = Team.find(params[:team_id])
        if team.users << user
            render plain: "Salvo"
        end
    end

    private
        def team_params
            params.permit(:title, :description)
        end
end