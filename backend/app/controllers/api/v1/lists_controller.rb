class Api::V1::ListsController < Api::V1::ApiController
    
    def index
        if params.has_key? :user_id or params.has_key? :team_id
            lists = get_owner.lists
        else
            lists = List.all
        end
        render json: lists
    end

    def create
        owner = get_owner
        list = owner.lists.create(list_params);

        if  list.save
            render json: list
        else
            render plain: "NOT SAVED"
        end
    end

    def show
        owner = get_owner
        render json: owner.lists.find(params[:id])
    end

    def update
        list = get_owner.lists.find(params[:id])
        list.update(list_params)
        if list.save
            render json: list 
        end
    end

    def destroy
        list = get_owner.lists.find(params[:id])
        if list.destroy
            render json: list
        end
    end
    
    private
    
    def list_params
        params.permit(:title, :description)
    end
        
        # def get_owner
        #     if params.has_key? :user_id
        #         owner = User.find(params[:user_id])
                
        #     elsif params.has_key? :team_id
        #         owner = Team.find(params[:team_id])
        #     end
            
        # end
end
