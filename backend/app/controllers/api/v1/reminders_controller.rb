class Api::V1::RemindersController < Api::V1::ApiController
    def create
        task = Task.find(params[:task_id])
        reminder = task.reminders.create(reminder_params)
        if reminder.save
            render json: reminder, status: :ok
        else
            render plain: "ok"
        end
    end

    def show
        task = Task.find(params[:task_id])
        render json: taks.reminders.find(params[:id])
    end

    private
    
    def reminder_params
        params.permit(:title, :description, :task_id)
    end
end
