class Api::V1::UserMailer < Api::V1::ApiMailer
  include Rails.application.routes.url_helpers
  def welcome_email
    @user = params[:user]
    mail(to: @user.email, subject: 'Welcome to Today\'s Mood')
  end

  def password_reset_email
    @user = params[:user]
    mail(to: @user.email, subject: 'Password reset')
  end
end
