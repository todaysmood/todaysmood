Rails.application.routes.draw do
  apipie

  namespace :api do
    namespace :v1 do
      get '/confirm_email/:token', to: "users#confirm_email", as: 'confirm_email'
      post "/auth/login", to: "users#login"
      post "/auth/signup", to: "users#create"
      post "/forgot", to: "users#forgot", as: 'forgot'
      post "/reset", to: "users#reset", as: 'reset'
      post "/reset/token", to: 'users#validate_token', as: 'reset/token'

      resources :tasks, only: [:index, :update, :show, :destroy, :create] do
        resources :tags, only: [:show, :destroy]
        resources :reminders
        resources :attachments
      end

      resources :users, only: [:index, :update, :show, :destroy] do
        resources :lists
        get "tasks_completed", to: "users#user_tasks_completed", as: 'tasks_completed'
        get "teams" => "users#user_teams"
        resources :tags
      end

      resources :teams do
        post "add_user/:user_id" => "teams#add_user"
        resources :tags
        resources :lists
      end

    end
  end
end
