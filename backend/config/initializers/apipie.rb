Apipie.configure do |config|
  config.translate         = false
  config.default_locale = nil 
  config.app_name                = "Todaysmood"
  config.api_base_url            = ""
  config.doc_base_url            = "/apipie"
  # where is your API defined?
  config.reload_controllers = true
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/api/v1/*.rb"

  config.validate = false
  config.default_version = '1'
  config.namespaced_resources = true
  config.ignored = %w[Api::V1::ApiController]
  config.reload_controllers = true if (Rails.env.development?)
end
