## Quickstart
É necessário um servidor PostgreSQL rodando na porta padrão.

Execute os seguintes comandos para iniciar o servidor Rails.

```
$ bundle install
$ rails db:setup
$ rails db:migrate
```

Para iniciar o servidor de backend, basta rodar:

```
$ rails server
```

## Diagrama

O modelo dos dados está em `db/` e pode ser alterado usando o
[StarUML](http://staruml.io/).
