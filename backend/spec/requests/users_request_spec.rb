require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :request do

    describe "POST /api/v1/auth/signup" do
       
        it 'returns a sucesseful subsribed user' do
            post api_v1_auth_signup_path, :params => {:name => 'washington', :email => 'luiz@luiz.com', :password => 'minhasenha', :format => :json}
            expect(response.status).to eql(201)  
        end
    end


    #describe "POST /api/v1/confirm_email" do
       
        #it "confirms registered user" do
            #token = extract_confirmation_token last_email
            #puts token
         #   get api_v1_confirm_email_path, :params => {token: 'luiz@luiz.com'}
          #  expect(response.status).to eql(202)
        # end
    #end
    
    describe 'POST /api/v1/auth/login' do
        it 'returns status not_found for unsubscribed user in login' do
            post api_v1_auth_login_path, :params => {:email => 'alguem@alguem.com', :password => 'minhasenha', :format => :json}
            expect(response.status).to eql(404) 
        end

        # it 'returns status ok for user login' do
        #     post api_v1_auth_login_path, :params => {:email => 'luiz@luiz.com', :password => 'minhasenha', :format => :json}
        #     puts JSON.parse(response.body)
        #     expect(response.status).to eql(200) 
        # end
       
    end

    #Testa
    describe '' do
        it '' do
        
        end
    end
end
