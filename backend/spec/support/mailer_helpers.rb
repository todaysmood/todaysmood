module MailerHelpers
    def last_email
      ActionMailer::Base.deliveries.last
    end

    def extract_confirmation_token(email)
      puts email
      email.body.to_s.match(/(?<=confirm_email\/).*?(?=">)/) if email && email.body
    end
end