require 'rails_helper'

RSpec.describe Task, type: :model do
    let(:owner) {User.create name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'}
    let(:list) {owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})}
    
    describe "Creating a task" do 
        it "Creates a task with everything ok" do
            task = Task.create({title: 'Estudar MAC0323', description: 'Passar', due: Date.tomorrow, 
                                estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            expect(task.save).to be_truthy
        end

        it "Should not create a task missing a parameter" do
            task = Task.create({title: 'Estudar MAC0323', due: Date.tomorrow, 
                estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            expect(task.save).to be_falsey
        end


        it "Should not create an user with invalid title" do
                task = Task.create({title: '', description: 'Passar', due: Date.tomorrow, 
                    estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
                expect(task.save).to be_falsey
                end            
    end

    describe "Searching a task: " do
        it "Find a registred user" do
            task = Task.create({title: 'Estudar MAC0323 ', description: 'Passar', due: Date.tomorrow,                      estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            expect(Task.find(task.id)).not_to be_nil
        end

        it "Should not find a unregistred task" do
            task = Task.create({title: 'Estudar MAC0323 ', description: 'Passar', due: Date.tomorrow,                      estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            expect{User.find(131231)}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "Updating a task information" do
        it "update a registred task" do 
            task = Task.create({title: 'Estudar MAC0323 ', description: 'Passar', due: Date.tomorrow,                      estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            task.update({title: 'Estudar Tecprog ', description: 'Entregar projeto', due: Date.tomorrow,                      estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            expect(task.save).to be_truthy
        end
    end

    describe "Deleting a task" do
        it "deletes an user" do
            task = Task.create({title: 'Estudar MAC0323 ', description: 'Passar', due: Date.tomorrow,                      estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id})
            id = task.id
            task.destroy
            expect{ Task.find(id)}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "should not delete a nonexistent user" do
            expect{Task.find(1233).destroy}.to raise_error(ActiveRecord::RecordNotFound)
        end

    describe "relationships" do
        it "should have tags" do
        expect(Task.new title: 'Estudar MAC0323', description: 'Passar', due: Date.tomorrow, estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id)
            .to respond_to :tags
        end

        it "should have team tag tasks" do
            expect(Task.new title: 'Estudar MAC0323', description: 'Passar', due: Date.tomorrow, estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id)
            .to respond_to :tag_tasks
        end

        it "should have reminders" do
            expect(Task.new title: 'Estudar MAC0323', description: 'Passar', due: Date.tomorrow, estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id)
            .to respond_to :reminders
        end

        it "should have attachments" do
            expect(Task.new title: 'Estudar MAC0323', description: 'Passar', due: Date.tomorrow, estimated_time: Date.tomorrow, priority: 1, completed: :true, list_id: list.id)
            .to respond_to :attachments
        end
    end
        
    end
end
