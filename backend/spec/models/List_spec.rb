require 'rails_helper'

RSpec.describe List, type: :model do
    let(:owner) {User.create name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'}
    describe "Creating a list" do 
        it "Creates an list with everything ok" do
            list = owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})
            expect(list.save).to be_truthy
        end

        it "Should not create an list missing a parameter" do
            list = owner.lists.create({title: 'Estudar'})
            expect(list.save).to be_falsey
        end

        it "Should not create an list with a very long title" do
            list = owner.lists.create({title: 'Estudar Labnum e Calculo Pi para terça-feira', description: 'Coisas para estudar'})
            expect(list.save).to be_falsey
        end

    end

    describe "Searching a list: " do
        it "Find a registred list" do
            list = owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})
            expect(owner.lists.find(list.id)).not_to be_nil
        end

        it "Find a unregistred list" do
            list = owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})
            expect{User.find(131231)}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "Updating a list" do
        it "update a registred list" do 
            list = owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})
            list.update({title: 'List model', description: 'Quais testes fazer'})
            puts list.title
            expect(list.save).to be_truthy
        end
    end

    describe "Deleting a list" do
        it "deletes an list" do
            list = owner.lists.create({title: 'Estudar', description: 'Coisas para estudar'})
            id = list.id
            list.destroy
            expect{ owner.lists.find(id)}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "should not delete a nonexistent list" do
            expect{owner.lists.find(1233).destroy}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "relationships" do
        it "should have tasks" do
            expect(owner.lists.new title: 'Estudar', description: 'Coisas para estudar')
            .to respond_to :tasks
        end
    end
end
