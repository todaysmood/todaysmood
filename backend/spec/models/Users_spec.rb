require 'rails_helper'

RSpec.describe User, type: :model do

    describe "Creating an user" do 
        it "Creates an user with everything ok" do
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'})
            expect(user.save).to be_truthy
        end

        it "Should not create an user missing a parameter" do
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com'})
            expect(user.save).to be_falsey
        end

        it "Should not create an user with invalid email" do
            user = User.create({name: 'Douglas', email: 'douglas.com', password: 'minhasenha'})
            expect(user.save).to be_falsey
        end

    end

    describe "Searching a user: " do
        it "Find a registred user" do
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'})
            expect(User.find(user.id)).not_to be_nil
        end

        it "Should not find a unregistred user" do
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'})
            expect{User.find(131231)}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "Updating an user information" do
        it "update a registred user" do 
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'})
            finder = User.find(user.id)
            newUser = User.update({name: 'Wander', email: 'wander@wander.com', password: '12345678'})
            expect(newUser.first.name != user.name && 
                newUser.first.email != user.password && 
                newUser.first.password != user.password).to be_truthy
        end
    end

    describe "Deleting an user" do
        it "deletes an user" do
            user = User.create({name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha'})
            id = user.id
            user.destroy
            expect{ User.find(id)}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "should not delete a nonexistent user" do
            expect{User.find(1233).destroy}.to raise_error(ActiveRecord::RecordNotFound)
        end

    describe "relationships" do
        it "should have lists" do
        expect(User.new name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha')
            .to respond_to :lists
        end

        it "should have team users" do
            expect(User.new name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha')
            .to respond_to :team_users
        end

        it "should have tags" do
            expect(User.new name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha')
            .to respond_to :tags
        end

        it "should have channels teams" do
            expect(User.new name: 'Douglas', email: 'douglas@douglas.com', password: 'minhasenha')
            .to respond_to :teams
        end
    end
        
    end
    
end