class CreateTagTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tag_tasks do |t|
      t.references :tag, :task
      t.timestamps
    end
  end
end
