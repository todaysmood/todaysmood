class CreateReminders < ActiveRecord::Migration[5.2]
  def change
    create_table :reminders do |t|
      t.string :title
      t.string :description
      t.date :date
      t.references :task
      t.timestamps
    end
  end
end
