class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string :title
      t.string :description
      t.references :taggable, polymorphic: true, index: true
      t.timestamps
    end
  end
end
