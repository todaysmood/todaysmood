class CreateTeamUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :team_users do |t|
      t.references :team, :user
      t.timestamps
    end
  end
end
