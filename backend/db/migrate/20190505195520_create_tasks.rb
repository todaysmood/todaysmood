class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title
      t.string :description
      t.datetime :due
      t.time :estimated_time
      t.integer :priority
      t.boolean :completed, :default => false
      t.references :list
      t.timestamps
    end
  end
end
