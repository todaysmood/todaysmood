# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# ruby encoding: utf-8
require 'json'
users = JSON.load File.open "db/seed_database/users.json", mode="r"
teams = JSON.load File.open "db/seed_database/teams.json", mode="r"
lists = JSON.load File.open "db/seed_database/lists.json", mode="r"
tasks = JSON.load File.open "db/seed_database/tasks.json", mode="r"
User.create(users)

Team.create(teams)

User.all.each do |user|
    ls = user.lists.create lists
    ls.each do |l|
        l.tasks.create tasks
    end
end

Team.all.each do |team|
    ls = team.lists.create  lists
    ls.each do |l|
        l.tasks.create tasks
    end
end
